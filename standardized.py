import os
import datetime as dt
import ZipFile from zipfile

data = dt.date.today()
os.makedirs('standardized-backup/{}'.format(data))

data_ext = 'raw-backup/{}/DADOS_RF.zip'.format(data), 'raw-backup/{}'.format(data)
with ZipFile(data_ext, "r") as extra:
   extra.extractall('standardized-backup/{}'.format(data))

os.rename('standardized-backup/{}/K3241.K03200Y0.D20212.EMPRECSV',"standardized-backup/{}/DADOS_RF.CSV".format(data))