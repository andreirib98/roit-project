import pandas as pd
import sqlalchemy
import os
import datetime as dt

data = dt.date.today()
os.makedirs('Conformized/{}'.format(data))

engine = sqlalchemy.create_engine(f'sqlite:///db/cnpj.db')

df = pd.Cov = pd.read_csv("standardized-backup/{}/DADOS_RF.CSV".format(data), encoding="ISO-8859-1", sep=';',
     names=['CNPJ Básico', 'Razão Social', 'Natureza Jurídica',
              'Qualificação do Responsável', 'Capital Social',
              'Porte da Empresa'])

print(df.head())
tipos_de_dados = pd.DataFrame(df.dtypes, columns=['Tipos de dados'])
tipos_de_dados.columns.name = 'Variáveis'

CNPJ = df['CNPJ Básico']
razaoSocial = df['Razão Social']
natJuridica = df['Natureza Jurídica']
qualResp = df['Qualificação do Responsável']
capSocial = df['Capital Social']
portEmpresa = df['Porte da Empresa']

df.to_sql('cnpj_basico', engine, if_exists ='append')