# ROIT-Project

Esse projeto tem como objetivo, criar uma pipeline de dados dos arquivos públicos de CNPJ do site da Receita Federal, processa-los e por fim disponibiliza-los em um banco de dados utilizando Python.

# Dados públicos da Receita Federal

Durante a implementação foi utilizado apenas uma das diversas opções de dados públicos que são disponibilizados, o link para informações de layouts e escolha de arquivos é o https://www.gov.br/receitafederal/pt-br/assuntos/orientacao-tributaria/cadastros/consultas/dados-publicos-cnpj ou você pode escolher pelo diretório disponibilizado por eles no link http://200.152.38.155/CNPJ/

## Pré-requisitos

python 3.10.2;
Bibliotecas request, pandas, sqlalchemy, zipfile;
DB Browser(SQLite)

## Configuração da pipeline

O projeto trabalha com o **agendador de tarefas dos Windows**, ou seja, ele funciona a partir de tasks que realizarão o processo semanalmente. Dentro do repositório existem duas pastas que são as principais para a configuração da pipeline.

A primeira configuração que deve ser feita é dentro da pasta **batch**, onde existem três arquivos denominados como as etapas da pipeline(raw, standardized e conformed) e um outro chamado **instructions.txt** que contém informações de como reescrever o caminho dos arquivos. Você deve editar os arquivos e corrigir o caminho onde estão localizados os arquivos na sua máquina.

    @echo off
    "C:\Users\"YOUR_USER"\AppData\Local\Programs\Python\Python39\python.exe" 
    "C:\Users\"YOUR_USER"\roit-project\raw.py"
    pause
Para o funcionamento é necessário que sejam corrigidos os três arquivos .bat que existem dentro da pasta.

## Utilizando a pipeline

Tendo os três arquivos .bat configurados corretamente, você deve acessar a pasta de tasks e executar os arquivos .xml que ela contém. As tasks tem programação semanal e serão executadas uma após a outra seguindo a ordem da pipeline. Ao fim da execução do arquivo Conformed, será criado dentro da pasta **db** uma arquivo chamado **cnpj.db**, que poderá ser importado com a informações sobre o arquivo baixado do site da Receita Federal. 

## Fluxograma

Também criei um fluxograma de como é ambiente ideal de funcionamento da pipeline.
https://miro.com/app/board/uXjVODROe5U=/?invite_link_id=526177008945
