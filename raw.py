import requests
import os
import datetime as dt

data = dt.date.today()
os.makedirs('raw-backup/{}'.format(data))

def data_download(url, end):
    res = requests.get(url)

    with open(end, 'wb') as new_file:
        new_file.write(res.content)

data_download('#http://200.152.38.155/CNPJ/K3241.K03200Y0.D20312.EMPRECSV.zip',
               'raw-backup/{}/DADOS_RF.zip'.format(data))



